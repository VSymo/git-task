@isTest 
private class NewAndExiistingControllerTest {
    @isTest static void NewAndExiistingControllerTest1(){
        PageReference pageRef = Page.ProductListPage;
        Test.setCurrentPage(pageRef);
        NewAndExistingController controller = new NewAndExistingController();
        String nextPage = controller.save().getUrl();
        System.assertEquals('/apex/ProductListPage', nextPage);
    }
    
    @isTest static void testWithId() {
        Product__c prod = new Product__c(Name='TestName1',
                                         Number__c=12,
                                         Price__c=120,
                                         Adding_Date__c=date.parse('2/10/2021'),
                                         Date_Of_Creation__c=date.parse('2/10/2021'));
        insert prod;
        PageReference pageRef = Page.ProductListPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', prod.id);
        NewAndExistingController controller = new NewAndExistingController();
        String nextPage = controller.save().getUrl();
        System.assertEquals('/apex/ProductListPage', nextPage);   
    }
}
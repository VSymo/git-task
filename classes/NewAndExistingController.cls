public class NewAndExistingController {
    
        public String methodA() {
        String methodA = 'method A from branch';
        }

    public String methodA() {
        String methodA = 'method A from branch';
    }

    public Product__c product { get; private set; }

    public NewAndExistingController() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        product = (id == null) ? new Product__c() : 
            [SELECT Id, Name, Price__c, Number__c, Type__c, Adding_Date__c, Date_Of_Creation__c, Availability__c 
             FROM Product__c 
             WHERE Id = :id
            ];
    }

    public PageReference save() {
        try {
            upsert(product);
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            return null;
        }
        PageReference redirectSuccess = new PageReference('/apex/ProductListPage');
        return (redirectSuccess);
    }
}
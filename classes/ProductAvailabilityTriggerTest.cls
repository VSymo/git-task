@isTest
public class ProductAvailabilityTriggerTest {
    @isTest static void TestAvailabilityWithZeroNumberOfProducts() {
        Product__c prod1 = new Product__c(Name='Car',
                                          Price__c=2000,
                                          Number__c=0);
        insert prod1;
        System.assert(!prod1.Availability__c);
    }
    @isTest static void TestAvailabilityWithSomeProducts() {
        Product__c prod = new Product__c(Name='Moto',
                                                      Price__c=200,
                                                      Number__c=24);
        insert prod;
        System.debug(prod.Availability__c + '   dva');
        System.assert(!prod.Availability__c);
    }
}
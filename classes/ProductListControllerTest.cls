@isTest 
private class ProductListControllerTest {
    @isTest static void dynamicSearch1(){
        ProductListPageController controller = new ProductListPageController();       
        controller.prId = 'Test';
        controller.prodName = 'prod';
        controller.prodAddingDate1 = date.parse('2/09/2021');
        controller.prodAddingDate2 = date.parse('2/12/2021');
        controller.dynamicSearch();
    }
    
    @isTest static void dynamicSerch2(){
        ProductListPageController controller = new ProductListPageController();
        controller.prodName = 'prod2';
        controller.dynamicSearch();
    }
    @isTest static void dynamicSerch3(){
        ProductListPageController controller = new ProductListPageController();
        controller.prodAddingDate1 = date.parse('2/09/2021');
        controller.prodAddingDate2 = date.parse('2/12/2021');
        controller.dynamicSearch();
    }
    @isTest static void sortMethod(){
        ProductListPageController controller = new ProductListPageController();   
		controller.sortByName();
        controller.sortByPrice(); 
        controller.sortByType();
        controller.sortByNumber();
        controller.sortByAddingDate();
        controller.sortByDateOfCreation();
        controller.sortByAvailability();
    }
    @isTest static void pagination(){
        ProductListPageController controller = new ProductListPageController();
        controller.PageNumber = 1;
        System.assertEquals(controller.PageNumber, 1);
        controller.PageSize = 5;
        controller.TotalPages = 10;
        controller.getPageNumberList();
    }
    
    @isTest static void testDelition() {
        Product__c prod = new Product__c(Name='TestName1',
                                        Number__c=12,
                                        Price__c=120,
                                        Adding_Date__c=date.parse('2/10/2021'),
                                        Date_Of_Creation__c=date.parse('2/10/2021'));
        insert prod;
        ProductListPageController controller = new ProductListPageController();
        controller.prId = prod.id;
        controller.delProduct();
    }
    @isTest static void testEdition() {
        Product__c prod = new Product__c(Name='TestName1',
                                                      Number__c=12,
                                                      Price__c=120,
                                                      Adding_Date__c=date.parse('2/10/2021'),
                                                      Date_Of_Creation__c=date.parse('2/10/2021'));
        insert prod;
        ProductListPageController controller = new ProductListPageController();
        controller.prId = prod.id;
        controller.editProduct();
        String nextPage = controller.editProduct().getUrl();
        System.assertEquals('/apex/NewProduct?id=' + prod.id, nextPage);
    }
}
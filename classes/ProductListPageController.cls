public class ProductListPageController {
    
    public String prodName {set;get;}   
    public List<Product__c> prod{set;get;} 
    public String prId {get;set;}
    private String sortOrder = 'Name';
    public Date prodAddingDate1 {set;get;} 
    public Date prodAddingDate2 {set;get;}
    
    
    public void dynamicSearch() {
        String searchQuery ='SELECT Id, Name, Price__c, Number__c, Type__c, ' +
            'Adding_Date__c, Date_Of_Creation__c, Availability__c ' +
            'FROM Product__c ';
        if(string.isNotBlank(prodName) && prodAddingDate1 != null && prodAddingDate2 != null)
        {
            searchQuery = searchQuery +' WHERE Name LIKE \'%' + prodName + 
                '%\' AND Adding_Date__c >= ' + String.valueOf(prodAddingDate1) + 
                ' AND Adding_Date__c <= ' + String.valueOf(prodAddingDate2);
        }
        else{
            if(string.isNotBlank(prodName))
            {
                searchQuery = searchQuery + ' WHERE Name LIKE \'%' + prodName + '%\'';
            }
            else
            {
                if(prodAddingDate1 != null && prodAddingDate2 != null)
                { 
                    searchQuery = searchQuery + ' WHERE Adding_Date__c >= ' + String.valueOf(prodAddingDate1) + 
                        ' AND Adding_Date__c <= ' + String.valueOf(prodAddingDate2);
                }
            }
        }
        List<Product__c> res = Database.query(searchQuery + 'ORDER BY ' + sortOrder + ' ASC');
        setList = new ApexPages.StandardSetController(res);
    }
    
    public List<Product__c> getProducts() {
        return (List<Product__c>) setList.getRecords();
    }
    
    public void sortByName() {
        this.sortOrder = 'Name';
        queryUpdate();
    }
    
    public void sortByPrice() {
        this.sortOrder = 'Price__c';
        queryUpdate();
    }
    
    public void sortByType() {
        this.sortOrder = 'Type__c';
        queryUpdate();
    }
    
    public void sortByNumber() {
        this.sortOrder = 'Number__c';
        queryUpdate();
    }
    
    public void sortByAddingDate() {
        this.sortOrder = 'Adding_Date__c';
        queryUpdate();
    }
    
    public void sortByDateOfCreation() {
        this.sortOrder = 'Date_Of_Creation__c';
        queryUpdate();
    }
    
    public void sortByAvailability() {
        this.sortOrder = 'Availability__c';
        queryUpdate();
    }
    
    public PageReference newProduct(){
        PageReference redirectSuccess = new PageReference('/apex/NewProduct');
        return (redirectSuccess);
    }
    
    public void delProduct() {
        Product__c prod = Database.query('SELECT Id, Name FROM Product__c WHERE Id = :prId LIMIT 1');
        delete prod;
        queryUpdate();
        getProducts();
    }
    
    public PageReference editProduct(){
        PageReference redirectSuccess = new PageReference('/apex/NewProduct?id=' + prId);
        return (redirectSuccess);
    }
    
    public void queryUpdate() { 
        String query = 'SELECT Id, Name, Price__c, Number__c, Type__c, ' +
            'Adding_Date__c, Date_Of_Creation__c, Availability__c ' +
            'FROM Product__c ' +
            'ORDER BY ' + sortOrder + ' ASC';
        setList = new ApexPages.StandardSetController(Database.query(query));
    }
    
    public list<SelectOption> getPageSizeList(){
        list<SelectOption>  options = new list<SelectOption>();
        options.add(new selectOption('5','5'));
        options.add(new selectOption('10','10'));
        options.add(new selectOption('25','25'));
        options.add(new selectOption('50','50'));
        options.add(new selectOption('100','100'));
        return options;
    }
    
    public list<SelectOption> getPageNumberList(){
        list<SelectOption> rowPageNumber = new list<SelectOption>();
        for (Integer i = 1; i <= TotalPages; i++) {
            String strInt = String.valueOf(i);
            rowPageNumber.add(new selectOption(strInt, strInt));
        }
        return rowPageNumber;
    }
    
    public ApexPages.StandardSetController setList {
        get {
            if (setList == null) {
                setList = new ApexPages.StandardSetController(Database.query(
                    'SELECT Id, Name, Price__c, Number__c, Type__c, ' +
                    'Adding_Date__c, Date_Of_Creation__c, Availability__c ' +
                    'FROM Product__c ' + 
                    'ORDER BY ' + sortOrder + ' ASC'));
            }
            if (this.PageSize == null) PageSize = 10;
            setList.setPageSize(PageSize);
            return setList;
        } set;
    }
    
    public Integer PageSize {
        get; set {
            if(value != null) this.PageSize = value;
        }
    }
    
    public Integer PageNumber {
        get {
            this.PageNumber = setList.getPageNumber();
            return this.PageNumber;
        }
        set {
            setList.setPageNumber(value);
        }
    }
    
    public Integer TotalPages {
        get {
            if (setList.getResultSize() <= 10)
                this.TotalPages = 1;
            if (Math.Mod ( setList.getResultSize(),setList.getPageSize() ) == 0)
                this.TotalPages = ( setList.getResultSize()/setList.getPageSize() );
            else this.TotalPages = ( setList.getResultSize()/setList.getPageSize() )+1;
            return totalpages;
        }
        set;
    }
}
trigger ProductAvailibilityTrigger on Product__c (before insert, before update ) {
    for (Product__c  product : Trigger.New){
        if (product.Number__c == 0){
            product.Availability__c = false;
        } else {
                product.Availability__c = true;
            }
        }
    }